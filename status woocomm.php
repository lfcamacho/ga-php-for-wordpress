/**
 * --------------------------------------------------------------------------------------------------
 * Shipment in progress add
 * --------------------------------------------------------------------------------------------------
 */

function wpblog_wc_register_post_statuses() {
	register_post_status('wc-shipping-progress', array(
		'label' => _x( 'Envio En Progreso', 'WooCommerce Order Status', 'text_domain' ),
		'public' => true,
		'exclude_from_search' => false,
		'show_in_admin_all_list' => true,
		'show_in_admin_status_list' => true,
		'label_count' => _n_noop( 'Envio En Progreso (%s)', 'Envio En Progreso (%s)', 'text_domain' )
	) );
}
add_filter('init', 'wpbloc_wc_register_post_statuses');

function wpblog_wc_add_order_statuses( $order_statuses ) {
	$order_statuses['wc-shipping-progress'] - _x( 'Envio En Progreso', 'WooCommerce Order status', 'text_domain' );
	return $order_statuses;
}
add_filter( 'wc_order_statuses', 'wpblog_wc_add_order_statuses' );
